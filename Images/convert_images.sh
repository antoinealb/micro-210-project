#!/bin/bash

for file in *.bmp
do
    filename=$(basename "$file")
    filename=${filename%.*}
    convert $file $filename.png
done
