; RPN calculator on a STK300
; (c) 2013 A. Albertelli, D. Konrad.
; Made for the MICRO-210 course by A. Schmidt,
; Supervisor : C. Meuwly
; Force et honneur

.include "m103def.inc"

; Reset adress
.org    0
    rjmp    reset

; External interrupt handler adress
.org    INT2addr
    rjmp    external_interrupt_2 

external_interrupt_2:
    ; Resets the MCU
    rjmp    0
    
    ; Should never happen
    reti    


.include "definitions.asm"
.include "macros.asm"
.include "math.asm"

.include "matrix.asm"
.include "vram.asm"
.include "assets.asm"

.include "lcd.asm"
.include "printf.asm"
.include "keyboard1.asm"
.include "stack.asm"


reset:

    ; Before anything, configure stack pointer
    ldi     r16, low(RAMEND)
    out     spl, r16 
    ldi     r16, high(RAMEND)
    out     sph, r16 

    ; Setup PORTB as an output (LED matrix is there)
    ldi     r16, 0xff
    out     DDRB, r16

    ; Debug port (for logic analyzer)
    sbi     DDRE, 2
    sbi     DDRE, 4


    ; Init the debug LCD 
    call    LCD_init

    ; Configures the LED matrix
    call    matrix_init

    ; Clear the VRAM
    call    clear_screen

    ; Inits the RPN stack
    call    stack_init

    ; Inits the AT keyboard
    call    keyboard_init

    ; Refresh the LED matrix
    call    send_screen

    ; Inits external interrupt (Port D Pin 2)
    ldi     r16, (1<<2) 
    out     EIMSK, r16
    sei

    ; Prints the adress of the RPN stack on the LCD
    call    LCD_clear
    call    LCD_home
    ldi     r18, low(stack)
    ldi     r19, high(stack)
    PRINTF  LCD
    .db "Stack =", HEX2, a, 0


    ; Clear the input buffer (16 bit)
    clr     r8
    clr     r9

    wait_ms 500


loop:

    ; Refresh screen
    call    send_screen

    ; Gets a keycode
    call    keyboard_get

    ; Pull down the 'waiting for key' pin
    cbi     PORTE, 4

    ; Pull up the 'data processing' pin
    sbi     PORTE, 2

    ; Tries to convert it to a number
    call    keyboard_get_value 

    ; Check if the key was in the keypad
    cpi     r22, 10
    brlo    PC+4
    call    not_a_number
    rjmp    PC+2
     
    call    is_a_number


    ; Writes the digit to the vram
    call    write_number
 
    ; Display the content of the buffer on the debug LCD
    push    r8
    push    r9 
    call    LCD_clear
    call    LCD_home
    pop     r9
    pop     r8
    PRINTF  LCD
    .db "Buffer : ", DEC2, 8, 0


    ; Pull down the 'data processing' key
    cbi     PORTE, 2

    ; Pull up the 'waiting for key' pin
    sbi     PORTE, 4

    ; Wait for the two release codes
    call    keyboard_get
    call    keyboard_get

    ; Clear the 'waiting for key' pin
    cbi     PORTE, 4

    ; Jumps back to beginning of the main loop
    jmp loop

    ; This function handles the case where the key is a number
is_a_number:
    ; Appends it to the buffer
    clr     r16
    clr     r17
    clr     r18

    ; Multiplies the buffer by 10
    add     r16, r8
    adc     r17, r9
    add     r16, r8
    adc     r17, r9
    add     r16, r8
    adc     r17, r9
    add     r16, r8
    adc     r17, r9
    add     r16, r8
    adc     r17, r9
    lsl     r16
    rol     r17

    ; Adds the key to the content of the buffer
    add     r16, r22
    adc     r17, r18

    mov     r8, r16
    mov     r9, r17
    ret


    ; This function handle the non-digit key press 
not_a_number:
    ; [enter] key
    cpi     r22, 90 
    brne    PC+2
    call    enter_pressed

    ; '+' key
    cpi     r22, 121
    brne    PC+2
    call    plus_pressed

    ; '-' key
    cpi     r22, 123
    brne    PC+2
    call    minus_pressed

    ; '*' key
    cpi     r22, 124
    brne    PC+2
    call    star_pressed

    ; '/' key
    cpi     r22, 0xe0
    brne    PC+2
    call    slash_pressed

    ; R key is for reset
    cpi     r22, 0x2d
    brne    PC+2
    jmp     0x00

    ; If it was another key, we ignore it 

    ; Write the input buffer to the screen
    ; If an operation key was pressed, it should contain zero
    call    write_number


    ; If there is nothing in the stack, dont display anything
    ; on the second line
    branch_if_data_available PC+2
    ret

    ; If there is some data in the stack, display it on the second line
    ; of the screen
    push    r8
    push    r9
    call    stack_get_top
    call    write_number_2nd_line
    pop     r9
    pop     r8

    ret


plus_pressed:
    call    do_addition
    ret

minus_pressed:
    call    do_substraction
    ret

star_pressed:
    call    do_multiplication
    ret

slash_pressed:
    call    do_division
    ret

    ; Handles the enter key
    ; pushes the content of the buffer on the stack and clears the bufffer.
enter_pressed:
    call    stack_push
    clr     r8
    clr     r9
    ret

    ; Handles the minus key
    ; Pops 2 operands, substract them and push them back
do_substraction:
    clr     r16
    cp      r8, r16
    cpc     r9, r16
    breq    PC+2
    call    stack_push

    branch_if_data_available PC+4
    clr     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop    
    

    mov     r16, r8
    mov     r17, r9


    branch_if_data_available PC+4
    clr     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop
    
    sub     r8, r16
    sbc     r9, r17 

    push    r8
    push    r9
    call    LCD_clear
    call    LCD_home

    pop     r9
    pop     r8
    PRINTF  LCD
    .db "Result :", DEC2, 8, 0

    call    stack_push

    clr     r8
    clr     r9
    ret

    ; Handles the plus key
    ; Pops 2 operands, adds them and push the results on the stack
do_addition:    
    clr     r16
    cp      r8, r16
    cpc     r9, r16
    breq    PC+2
    call    stack_push

    branch_if_data_available PC+4
    clr     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop    
    

    mov     r16, r8
    mov     r17, r9


    branch_if_data_available PC+4
    clr     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop
    
    add     r8, r16
    adc     r9, r17 

    push    r8
    push    r9

    call    LCD_clear
    call    LCD_home

    pop     r9
    pop     r8
    PRINTF  LCD
    .db "Result:", DEC2, 8, 0

    call    stack_push

    clr     r8
    clr     r9
    ret

    ; Handles the '*' key.
    ; Pops 2 operands, multiply them and push the product to the stack
do_multiplication:
    clr     r16
    cp      r8, r16
    cpc     r9, r16
    breq    PC+2
    call    stack_push
 
    branch_if_data_available PC+4
    clr     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop    

    mov     a0, r8
    mov     a1, r9


    branch_if_data_available PC+5
    clr     r8
    inc     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop    

    mov     b0, r8
    mov     b1, r9

    call    mul22

    push    r8
    push    r9
    call    LCD_clear
    call    LCD_home

    pop     r9
    pop     r8
    PRINTF  LCD
    .db DEC2, a,"*",DEC2, b, "=", DEC2, c, 0

    call    stack_push

    clr     r8
    clr     r9
    ret


    ; Handles the '/' key
    ; Pops two operands, divide them and push the result on the stack
do_division:

    clr     r16
    cp      r8, r16
    cpc     r9, r16
    breq    PC+2
    call    stack_push
 
    branch_if_data_available PC+4
    clr     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop    

    mov     b0, r8
    mov     b1, r9

    branch_if_data_available PC+5
    clr     r8
    inc     r8
    clr     r9
    rjmp    PC+2 
    call    stack_pop    

    mov     a0, r8
    mov     a1, r9


    call    div22


    push    a0
    push    a1
    push    b0
    push    b1
    push    c0 
    push    c1
    call    LCD_clear
    call    LCD_home

    pop     c1
    pop     c0
    pop     b1
    pop     b0
    pop     a1
    pop     a0
    PRINTF  LCD
    .db DEC2, a, "/", DEC2, b, "=", DEC2, c, 0

    call    stack_push
    clr     r8
    clr     r9

    call    keyboard_get
    

    ret
