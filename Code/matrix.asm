; Driver for a 2416 Dot Matrix Display (Sure Electronics)
; (c) 2013 A. Albertelli, D. Konrad.


; Command codes
.equ COMMAND_MODE   = 0b100
.equ READ_MODE      = 0b110
.equ WRITE_MODE     = 0b101

; Commands
.equ CMD_SYSDIS     = 0x00    ; Turn both oscillator off
.equ CMD_SYSEN      = 0x01    ; Turn off system oscillator
.equ CMD_LEDOFF     = 0x02    ; Turn off LED duty cycle generator
.equ CMD_LEDON      = 0x03    ; Turn on LED duty cycle generator
.equ CMD_BLINKOFF   = 0x08    ; Turn off blinking function
.equ CMD_BLINKON    = 0x09    ; Turn on blinking function
.equ CMD_SLAVEMODE  = 0x10    ; Set slave mode and external clock
.equ CMD_MASTERMODE = 0x14    ; Set master mode and internal source
.equ CMD_RCCLK      = 0x18    ; Set clock to internal RC
.equ CMD_EXTCLK     = 0x1c    ; Set clock to external clock
.equ CMD_PMOS       = 0x2c    ; Setup PMOS
.equ CMD_PWM        = 0xa0    ; PWM command. Must be ORd with the PWM level

.equ CS_PORT        = PORTB   ; Port for the chip select 
.equ CS_BIT         = 0
.equ DATA_PORT      = PORTB   ; Port for the Data pin
.equ DATA_BIT       = 4       
.equ WR_PORT        = PORTB   ; Port for the WR pin 
.equ WR_BIT         = 2      

; Take the control of the LED matrix (pulls CS to zero)
.macro cs_take 
    cbi     CS_PORT, CS_BIT
.endmacro

; Release control of the LED matrix (pull CS to one)
.macro cs_free
    sbi     CS_PORT, CS_BIT
.endmacro

; Push the first n LSBs of arg to the matrix board (MSB first)
; param : [x] The data to shift.
; N the number of bits to shift 
.macro send_data_to_matrix
    push    r16
    push    r17

    ldi     r16, @0
    ldi     r17, 1<<(@1-1)

    call    _send_data_to_matrix

    pop     r17
    pop     r16
.endmacro

; Sends the content of a register to the matrix
; Param : the register to send
; Param : the number of bits to send (immediate value)
.macro send_register_to_matrix
    push    r16
    push    r17

    mov     r16, @0
    ldi     r17, (1<<(@1-1))
    call    _send_data_to_matrix

    pop     r17
    pop     r16
    
.endmacro

; Sends a command to the matrix
; Param : The command to send (eg : CMD_LEDON)
.macro send_command_to_matrix
    cs_take
    send_data_to_matrix COMMAND_MODE, 3 ;  Command header
    send_data_to_matrix @0, 8 ; actual command
    send_data_to_matrix 1, 1 ; One extra dont care bit
    cs_free
.endmacro



; Shifts out the given bits to the matrix, MSB first
; Parameters :
; r16 Data to shift out 
; r17 number of bits to move 
; Do not call it directly, use the helper macros instead
_send_data_to_matrix:
    ; Sets the clock to one
    cbi     WR_PORT, WR_BIT

    ; Adds some delay
    nop
    nop
    nop
    nop

    push    r16

    ; Setups the data pin
    and     r16, r17
    brne    _data_is_one  
    cbi     DATA_PORT, DATA_BIT
    rjmp    _end_of_data
_data_is_one:
    sbi     DATA_PORT, DATA_BIT
_end_of_data:

    ; Adds some delay
    nop
    nop
    nop
    nop

    ; Puts the clock to zero
    sbi     WR_PORT, WR_BIT

    ; prepares next bit
    pop     r16

    ; If there is something left to shift, loop again
    lsr     r17
    brne    _send_data_to_matrix 
    ; Else, return
    ret 

; Inits the whole matrix.
; Parameters : none
matrix_init:

    ; Matrix setup sequence
    send_command_to_matrix CMD_SYSDIS ; Disable all
    send_command_to_matrix CMD_PMOS
    send_command_to_matrix CMD_MASTERMODE
    send_command_to_matrix CMD_SYSEN
    send_command_to_matrix CMD_LEDON
    send_command_to_matrix CMD_PWM+15
    ret 
