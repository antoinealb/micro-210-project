; Interface for the AT keyboard
; (c) 2013 A. Albertelli, D. Konrad.


.macro	DETECT_10 ; port,pin,timeout-addr	; Wait for 1-0 transition	
	clr	    r16	    	; reset timout counter
p0_%:	
    dec 	r16	    	; decrement timeout counter
	breq	@2	    	; jump to timeout-addr if 0
	sbis	@0, @1		; loop back if 0, skip if 1
	rjmp	p0_%

	clr 	r16	    	; reset timout counter
p1_%:	
    dec 	r16	    	; decrement timeout counter
	breq	@2	    	; jump to timeout-addr if 0
	sbic	@0, @1		; loop back if 1, skip if 0
	rjmp	p1_%
.endmacro

keyboard_get:	
    push    r17
keyboard_get_restart:
    clr     r22

	DETECT_10 PIND,KB_CLK,keyboard_get_restart	; detect start-bit

	ldi 	r17,8

loop_kbd_get:

	DETECT_10 PIND,KB_CLK,keyboard_get_restart	; detect data-bit
	P2C	    PIND,KB_DAT		; pin to carry
	ror	    r22			; roll carry to MSB
	dec	    r17
	brne	loop_kbd_get
	DETECT_10 PIND,KB_CLK,keyboard_get_restart	; detect parity-bit
	DETECT_10 PIND,KB_CLK,keyboard_get_restart	; detect stop-bit

    pop r17
    ret



keyboard_init:
    cbi     DDRD, KB_DAT
    cbi     DDRD, KB_CLK
    sbi     PORTD, KB_DAT
    sbi     PORTD, KB_CLK
    ret


keyboard_get_value:
    cpi     r22, 112
    brne    not_0
    ldi     r22, 0
    ret
not_0:
    cpi     r22, 105
    brne    not_1
    ldi     r22, 1
    ret
not_1:
    cpi     r22, 114
    brne    not_2
    ldi     r22, 2
    ret
not_2:
    cpi     r22, 122
    brne    not_3
    ldi     r22, 3
    ret
not_3:
    cpi     r22, 107
    brne    not_4
    ldi     r22, 4
    ret
not_4:
    cpi     r22, 115
    brne    not_5
    ldi     r22, 5
    ret
not_5:
    cpi     r22, 116
    brne    not_6
    ldi     r22, 6
    ret
not_6:
    cpi     r22, 108
    brne    not_7
    ldi     r22, 7
    ret
not_7:
    cpi     r22, 117
    brne    not_8
    ldi     r22, 8
    ret
not_8:
    cpi     r22, 125
    brne    not_9
    ldi     r22, 9
    ret
not_9:
    ret
