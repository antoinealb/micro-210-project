; (c) 2013 A. Albertelli, D. Konrad
; This files provides everything needed to implement a 'video ram'
; for the LED matrix. Everything written in the VRAM will be copied
; on the device at the next refresh. The VRAM is organized using a byte
; for each group of 4 led, wasting 4 bits, but avoiding binary masking.
; 
; The adressing go like this
; 0x05------------------------------0x0
;  .                                  .
;  .                                  .
;  .                                  .
;  .                                  .
; 0x5f------------------------------0x5a 
; So the topmost, leftmost LED is the bit 0 of the byte 0

; Size of the VRAM (384 leds, without using high nibble)
.equ VRAM_SIZE =96

.dseg ; Switch to data segment (RAM)
vram:
    .byte   VRAM_SIZE 
vram_end:
.cseg


; Writes a byte to a specific adress in VRAM
; Parameters :
; 1) Offset in table to write
; 2) Value to write (immediate) 
.macro write_in_vram
    push    zh
    push    zl
    push    r16

    ldi     zh, high(vram + @0)
    ldi     zl, low(vram + @0)
    ldi     r16, @1
    st      z, r16

    pop     r16
    pop     zl
    pop     zh
.endmacro

; Write an 8x8 pattern from flash to the VRAM
; Parameters :
; 1) X Block on which the pattern should be written (0-2)
; 2) y Block on which the pattern should be written (0-1)
; 3) Adress of the patern in the dseg
.macro write_pattern
    push    zh ; holds the adress of the current byte from ROM
    push    zl
    push    yh ; holds the current adress in the VRAM
    push    yl
    push    r0 ; current byte
    push    r17 ; loop counter
    
    clr     r17
    ldi     zh, high(2*@2)
    ldi     zl, low(2*@2)
    ldi     yh, high(vram + 2*@0 + 32*@1)
    ldi     yl, low(vram + 2*@0 + 32*@1)

    call    write_loop

    pop     r17
    pop     r0
    pop     yl
    pop     yh
    pop     zl
    pop     zh
.endmacro

; Counts N patterns from the given one and draws the resulting one
; parameters
; 1) base adress
; 2) number of offset in a register
; 3) coordinate on the matrix
.macro write_pattern_with_offset
    push    zh ; holds the adress of the current byte from ROM
    push    zl
    push    yh ; holds the current adress in the VRAM
    push    yl
    push    r0 ; current byte
    push    r17 ; loop counter

    clr     r17
    ldi     zh, high(2*@0)
    ldi     zl, low(2*@0)

    ; Multiplies the offset by 16
    mov     yl, @1
    lsl     yl
    rol     yh
    lsl     yl
    rol     yh
    lsl     yl
    rol     yh
    lsl     yl
    rol     yh

    add     zl, yl
    adc     zh, yh 

    ldi     yh, high(vram + 2*@2 + 32*@3)
    ldi     yl, low(vram + 2*@2 + 32*@3)

    call    write_loop

    pop     r17
    pop     r0
    pop     yl
    pop     yh
    pop     zl
    pop     zh
.endmacro


; Displays a number on the second line of the matrix.
; Parameters :
; The number is in to be in r8 and r9
write_number_2nd_line:
    push    r8
    push    r9
    push    a0
    push    a1
    push    b0
    push    b1

    ; clear the display zone
    write_pattern 1,0, empty_png
    write_pattern 1,1, empty_png
    write_pattern 1,2, empty_png

    ldi     a0, low(1000)
    ldi     a1, high(1000)
    cp      r8, a0 
    cpc     r9, a1
    brlo    lower_than_999_2nd_line
    write_pattern 1, 2, infinite_png
    jmp     end_2nd_line
lower_than_999_2nd_line:

    mov     a0, r8
    mov     a1, r9

    ldi     b0, 100
    clr     b1

    call    div22

    ; we use the t flag to indicate number if bigger than 100
    clt

    tst     c0
    breq    lower_than_100_2nd_line

    ; we use the t flag to indicate number if bigger than 100
    set
    
    ; Now c0:c1 contains the result
    write_pattern_with_offset c0_png, c0, 1, 0
lower_than_100_2nd_line:

    ; d contains the reminder
    mov     a0, d0 
    mov     a1, d1
    ldi     b0, 10

    call    div22

    ; if the number is bigger than 99, write the second digit
    ; even if it is zero
    brts    PC+3 
    tst     c0
    breq    lower_than_10_2nd_line

    write_pattern_with_offset c0_png, c0, 1, 1
lower_than_10_2nd_line:
    write_pattern_with_offset c0_png, d0, 1, 2 

end_2nd_line:
    pop     b1
    pop     b0
    pop     a1
    pop     a0
    pop     r9
    pop     r8
    ret

; Displays a number on the second line of the matrix.
; Parameters :
; The number is in to be in r8 and r9
write_number:
    push    r8
    push    r9
    push    a0
    push    a1
    push    b0
    push    b1

    ; clear the display zone
    write_pattern 0,0, empty_png
    write_pattern 0,1, empty_png
    write_pattern 0,2, empty_png

    ldi     a0, low(1000)
    ldi     a1, high(1000)
    cp      r8, a0 
    cpc     r9, a1
    brlo    lower_than_999
    write_pattern 0, 2, infinite_png
    jmp     end
lower_than_999:

    mov     a0, r8
    mov     a1, r9 

    ldi     b0, 100
    clr     b1

    call    div22

    ; we use the t flag to indicate number if bigger than 100
    clt

    tst     c0
    breq    lower_than_100

    ; we use the t flag to indicate number if bigger than 100
    set
    
    ; Now c0:c1 contains the result
    write_pattern_with_offset c0_png, c0, 0, 0
lower_than_100:

    ; d contains the reminder
    mov     a0, d0 
    mov     a1, d1
    ldi     b0, 10

    call    div22

    ; if the number is bigger than 99, write the second 
    ; digit even if it is zero
    brts    PC+3 
    tst     c0
    breq    lower_than_10

    write_pattern_with_offset c0_png, c0, 0, 1
lower_than_10:
    write_pattern_with_offset c0_png, d0, 0, 2 

end:
    pop     b1
    pop     b0
    pop     a1
    pop     a0
    pop     r9
    pop     r8
    ret

write_loop:

    lpm 
    adiw    zh:zl, 1 ; increment z pointer
    st      y+, r0 


    push    r17
    andi    r17, 0x01
    pop     r17
    
    breq    PC+2
    ; If we are here, it means we need to go to next line
    adiw    yh:yl, 2
    
    inc     r17


    cpi     r17, 16
    brne    write_loop
    ret


; Clear the VRAM
; Parameters : none
clear_screen:
    ldi     zl, low(vram)
    ldi     zh, high(vram)
    
    clr     r16

clear_screen_loop:

    st     z+, r16

    ; Test if we reached the end of VRAM
    ldi     yl, low(vram_end)
    ldi     yh, high(vram_end)
    sub     yl, zl
    sbc     yh, zh
    brne    clear_screen_loop  
    ret 

; Sends the whole VRAM to the matrix
; Parameters : none
send_screen:
    push    zl  
    push    zh
    push    yl
    push    yh
    push    r16
    push    r17

    ldi     zl, low(vram)
    ldi     zh, high(vram)

    clr     r17 ; r17 holds the adress on the screen

send_screen_loop:
    ld      r16, z+
    
    cs_take
    send_data_to_matrix 0x05, 3 ; write mode
    send_register_to_matrix r17, 7
    send_register_to_matrix r16, 4
    cs_free


    inc     r17
    cpi    r17, VRAM_SIZE
    brne    send_screen_loop  

    pop     r17
    pop     r16
    pop     yh
    pop     yl
    pop     zh
    pop     zl

    ret
