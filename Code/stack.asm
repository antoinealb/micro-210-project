; Implementation a 16-bit stack
; (c) 2013 A. Albertelli, D. Konrad


; Stack of the size in bytes (dont forget that
; we store 16 bit words)
.equ    STACKSIZE = 64

.dseg
stack:
    .byte   STACKSIZE
.cseg

; Branch if there are datas in the stack
; Parameter : the label to branch to.
.macro  branch_if_data_available
    push    r16
    ldi     r16, high(stack)
    cpi     xl, low(stack)
    cpc     xh, r16
    pop     r16
    brne    @0
.endmacro

; Inits the stack pointer (X)
; Parameters : none
stack_init:
    ldi     xl, low(stack)
    ldi     xh, high(stack)
    ret

; Push a word on the stack.
; Parameters : the word to push in r8:r9 (c0:c1)
stack_push:
    st      x+, r8
    st      x+, r9
    ret

; Pops a word off the stack
; Parameters : none
; Returns : the word, in r8:r9
stack_pop:
    ld      r9, -x
    ld      r8, -x
    ret

; Gets the top of the stack, without popping it
; Parameters : none
; returns : the word, in r8:r9
stack_get_top:
    push    yl
    push    yh
    clr     r8
    clr     r9
    mov     yl, xl
    mov     yh, xh
    sbiw    yl, 2

    branch_if_data_available PC+2 
    rjmp    PC+3
    ld      r8, y
    ldd     r9, Y+1
    pop     yh
    pop     yl
    ret


