#!/usr/bin/env python2
# Converts an image from PNG to our own binary format.
# (c) 2013 A. Albertelli, D. Konrad
import sys, png
from os.path import basename

def main():
    if len(sys.argv) < 2:
        print "Provide image name"
        return
        

    picture = png.Reader(sys.argv[1])

    (width, height, pixels, metadata) = picture.read()

    if width != 8 or height != 8:
        print "Invalid size (must be 8x8)"
        return

    (width, height, pixel_gen, meta) = picture.asFloat()

    pixels = []


    for i in pixel_gen:
        # If we got several colors, we only keep red
        if len(i) is 24:
            i = i[0::3] 
        line = sum([(1-int(v+0.5) << u) for u, v in enumerate(i)])
     
        pixels.append(line)


    result = []

    for line in pixels:
        result.append((line >> 4) & 0xf)
        result.append(line & 0xf)


    labelname = basename(sys.argv[1]).replace(".", "_")+":"
    if labelname[0].isdigit():
        labelname = "c" + labelname
    
    # Print label
    print labelname

    # Prints a nicely indented line with everything in hex
    print 4*" "+".db "+", ".join(map(hex, result[:8]))
    print 4*" "+".db "+", ".join(map(hex, result[8:]))


if __name__=="__main__":
    main()
