Led matrix connector pinout (goes on PORTB)
===========================================

| Pin | Function | Description                     |
| ----|----------|---------------------------------|
| 1   | CS1      | Chip select 1                   |
| 2   | CS2      | Chip select 2                   |
| 3   | WR       | Write clock                     |
| 4   | RD       | Read clock                      |
| 5   | DATA     | Data                            |
| 6   | GND      | Ground                          |
| 7   | OSC      | External clock, connect to GND  |
| 8   | SYNC     | For cascading stuff             |
| 9   | GND      | Ground                          |
| 10  | +5V      |                                 |

STK300 Port connector pinout
============================

| Pin | Function|
| ----|-----------------|
| 1   | GPIO Port, bit 0|
| 2   | GPIO Port, bit 1|
| 3   | GPIO Port, bit 2|
| 4   | GPIO Port, bit 3|
| 5   | GPIO Port, bit 4|
| 6   | GPIO Port, bit 5|
| 7   | GPIO Port, bit 6|
| 8   | GPIO Port, bit 7|
| 9   | GND             |
| 10  | +5V             |


Port usage
==========

| Port  | Function                    |
| ------|-----------------------------|
| PORTF | Analog input                |
| PORTB | Led Matrix interface / LEDs |


Useful links
============
* http://forum.arduino.cc/index.php/topic,21819.0.html
